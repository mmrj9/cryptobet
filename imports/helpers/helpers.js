import moment from 'moment-timezone';
import jstz from 'jstz';
import {Session} from 'meteor/session';



const getTotalWheelTurnValue = (bets, username) => {
  let result = 0;
  bets.forEach((bet) => {
    if (!username || username == bet.user) {
      bet.bets.forEach((b) => {
        result = result + b.value;
      });
    }
  });
  return result.toFixed(2);
}

const convertDateToUserTimezone = (date) => {
  const user = Meteor.user();

  if (user && user.profile.preferences && user.profile.preferences.timezone) {
    return moment.tz(date, user.profile.preferences.timezone).toDate();
  } else {
    const timezone = jstz.determine();
    if (timezone) {
      return moment.tz(date, timezone.name()).toDate();
    } else {
      return date;
    }
  }
}

const convertToUserCurrency = (value) => {
  const userConversionRate = Session.get("currencyConversionRate") || 1;
  return Number((value * userConversionRate).toFixed(2));
}

const getUserCurrencySymbol = (value) => {
  const userCurrencySymbol = Session.get("currencySymbol") || defaultCurrencySymbol;
  return userCurrencySymbol;
}

export {getTotalWheelTurnValue, convertDateToUserTimezone, convertToUserCurrency, getUserCurrencySymbol};

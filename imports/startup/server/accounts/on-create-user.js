import {Accounts} from 'meteor/accounts-base';

Accounts.onCreateUser((options, user) => {
  const userToCreate = user;
  if (options.profile)
    userToCreate.profile = options.profile;
  return userToCreate;
});

Accounts.validateLoginAttempt(function(attemptInfo) {

  if (attemptInfo.type == 'resume')
    return true;

  if (attemptInfo.methodName == 'createUser')
    return false;

  if (attemptInfo.methodName == 'login' && attemptInfo.allowed) {
    var verified = false;
    var email = attemptInfo.methodArguments[0].user.email;
    attemptInfo.user.emails.forEach(function(value, index) {
      if (email == value.address && value.verified)
        verified = true;
      }
    );
    if (!verified)
      throw new Meteor.Error(403, 'Verify Email first!');
    }

  return true;
});

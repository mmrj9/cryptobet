import '../../api/Users/server/methods';
import '../../api/Users/server/publications';

import '../../api/CryptoCurrencies/server/methods';
import '../../api/CryptoCurrencies/server/publications';
import '../../api/CryptoCurrencies/server/default';

import '../../api/Currencies/server/methods';
import '../../api/Currencies/server/publications';

import '../../api/PlatformEarnings/server/methods';
import '../../api/PlatformEarnings/server/publications';

import '../../api/WheelTurns/server/methods';
import '../../api/WheelTurns/server/publications';

import '../../api/Utility/server/methods';

import React from 'react';
import PropTypes from 'prop-types';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import {Alert, Button} from 'reactstrap';
import {ReactSlackChat} from 'react-slack-chat';
import {Meteor} from 'meteor/meteor';
import {Session} from 'meteor/session';
import Currencies from '../../../api/Currencies/Currencies';
import {createContainer} from 'meteor/react-meteor-data';
import {Roles} from 'meteor/alanning:roles';
import {Bert} from 'meteor/themeteorchef:bert';
import Navigation from '../../components/Navigation/Navigation';
import Authenticated from '../../components/Authenticated/Authenticated';
import Public from '../../components/Public/Public';
import Index from '../../pages/Index/Index';
import Wheel from '../../pages/Wheel/Wheel';
import Signup from '../../pages/Signup/Signup';
import Login from '../../pages/Login/Login';
import Logout from '../../pages/Logout/Logout';
import VerifyEmail from '../../pages/VerifyEmail/VerifyEmail';
import RecoverPassword from '../../pages/RecoverPassword/RecoverPassword';
import ResetPassword from '../../pages/ResetPassword/ResetPassword';
import Profile from '../../pages/Profile/Profile';
import NotFound from '../../pages/NotFound/NotFound';
import Footer from '../../components/Footer/Footer';
import Terms from '../../pages/Terms/Terms';
import Privacy from '../../pages/Privacy/Privacy';
import BackOffice from '../../pages/BackOffice/BackOffice';

import './App.scss';

class App extends React.Component {
  constructor(props) {
    super(props)

    this.handleResendVerificationEmail = this.handleResendVerificationEmail.bind(this);
  }

  componentDidUpdate() {
    let user = Meteor.user();
    if (user && user.profile.preferences && user.profile.preferences.currency && (!Session.get('currencyConversionRate') || !Session.get('currencySymbol'))) {
      let currency = Currencies.findOne({code: user.profile.preferences.currency});
      if (currency) {
        Session.set('currencyConversionRate', currency.rate);
        Session.set('currencySymbol', currency.symbol);
      }
    }
  }

  handleResendVerificationEmail = (emailAddress) => {
    Meteor.call('users.sendVerificationEmail', (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert(`Check ${emailAddress} for a verification link!`, 'success');
      }
    });
  };

  render() {
    let props = this.props;
    return (
      <Router>
        {
          !props.loading
            ? <div className="App">
                {
                  props.userId && !props.emailVerified
                    ? <Alert className="verify-email text-center">
                        <p>Hey friend! Can you
                          <strong>
                            verify your email address
                          </strong>
                          ({props.emailAddress}) for us?
                          <Button color="secondary" onClick={() => handleResendVerificationEmail(props.emailAddress)} href="#">
                            Re-send verification email</Button>
                        </p>
                      </Alert>
                    : ''
                }
                {
                  Meteor.user()
                    ? <ReactSlackChat
                        botName={Meteor.user().profile.username}
                        apiToken={Meteor.settings.public.SLACK.apiToken}
                        channels={[{
                            name: 'general',
                            icon: 'https://material.io/guidelines/static/spec/images/callouts/default.svg'
                          }
                        ]}
                        helpText='Chat with us'
                        themeColor='#2b2a2a'
                        userImage='http://www.iconshock.com/img_vista/FLAT/mail/jpg/robot_icon.jpg'
                        debugMode={false}
                        closeChatButton={true}
                        hooks={[{/* My Custom Hook */
                            id: 'getSystemInfo',
                            action: () => 'MY SYSTEM INFO!'
                          }
                        ]}/>
                    : <div/>
                }
                <Navigation {...props}/>
                <Switch>
                  <Route exact={true} name="index" path="/" component={Index}/>
                  <Authenticated exact={true} path="/profile" component={Profile} {...props}/>
                  <Public name="wheel" path="/wheel" component={Wheel}/>
                  <Public path="/signup" component={Signup} {...props}/>
                  <Public path="/login" component={Login} {...props}/>
                  <Public path="/logout" component={Logout} {...props}/>
                  <Route name="verify-email" path="/verify-email/:token" component={VerifyEmail}/>
                  <Route name="recover-password" path="/recover-password" component={RecoverPassword}/>
                  <Route name="reset-password" path="/reset-password/:token" component={ResetPassword}/>
                  <Route name="terms" path="/terms" component={Terms}/>
                  <Route name="privacy" path="/privacy" component={Privacy}/> {
                    props.roles.indexOf("admin") != -1
                      ? <Route name="bo" path="/bo" component={BackOffice}/>
                      : null

                  }
                  <Route component={NotFound}/>
                </Switch>
                <Footer/>
              </div>
            : ''
        }
      </Router>
    );
  }
}
App.defaultProps = {
  userId: '',
  emailAddress: ''
};

App.propTypes = {
  loading: PropTypes.bool.isRequired,
  userId: PropTypes.string,
  emailAddress: PropTypes.string,
  emailVerified: PropTypes.bool.isRequired
};

const getUserName = name => (
  {string: name, object: `${name.first} ${name.last}`}[typeof name]
);

export default createContainer(() => {
  const loggingIn = Meteor.loggingIn();
  const user = Meteor.user();
  const userId = Meteor.userId();
  const subscriptionCurrencies = Meteor.subscribe('currencies');
  const loading = !Roles.subscription.ready() && !subscriptionCurrencies.ready();
  const name = user && user.profile && user.profile.name && getUserName(user.profile.name);
  const emailAddress = user && user.emails && user.emails[0].address;

  return {
    loading,
    loggingIn,
    authenticated: !loggingIn && !!userId,
    name: name || emailAddress,
    roles: !loading && Roles.getRolesForUser(userId),
    userId,
    emailAddress,
    emailVerified: user && user.emails
      ? user && user.emails && user.emails[0].verified
      : true
  };
}, App);

import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import _ from 'underscore';
import {createContainer} from 'meteor/react-meteor-data';
import {Row, Col, FormGroup, Input, Button} from 'reactstrap';
import Table from 'rc-table';

import PlatformEarnings from '../../../api/PlatformEarnings/PlatformEarnings';
import CryptoCurrencies from '../../../api/CryptoCurrencies/CryptoCurrencies';

import './BackOffice.scss';
import {convertToUserCurrency, getUserCurrencySymbol, convertDateToUserTimezone} from '../../../helpers/helpers';
import {groupEarningsByCurrency} from '../../../api/PlatformEarnings/helpers';

const columns = [
  {
    title: 'Timestamp',
    dataIndex: 'timestamp',
    key: 'timestamp',
    width: 130,
    render: (value) => moment(value).fromNow()
  }, {
    title: 'Action',
    dataIndex: 'action',
    key: 'action',
    width: 100
  }, {
    title: 'Data',
    dataIndex: 'data',
    key: 'data',
    width: 170
  }, {
    title: 'Earnings',
    dataIndex: 'earnings',
    key: 'earnings',
    width: 250,
    render: (value) => value.map((v, index) => {
      return <div key={index}>
        <span>{`${v.currency}: ${v.amount}`}</span><br/></div>
    })
  },
  // {   title: 'Operations', dataIndex: '', key:'operations', render: () => <a href="#">Delete</a>, }
];
class BackOffice extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedCurrency: "",
      userToTransfer: "",
      withdrawAddress: "",
      amount: 0,
      currencySearch: ""
    }

    this.getPlatformEarningsData = this.getPlatformEarningsData.bind(this);
    this.getEarnings = this.getEarnings.bind(this);
    this.transfer = this.transfer.bind(this);
    this.withdraw = this.withdraw.bind(this);
  }

  componentWillMount() {
    // console.log("componentWillMount");
  }

  componentDidMount() {}

  componentWillUpdate() {
    // console.log("componentWillUpdate");
  }

  getPlatformEarningsData() {
    return PlatformEarnings.find({}).fetch();
  }

  transfer() {
    Meteor.call(
      "transfer.to.user",
      this.state.amount,
      this.state.selectedCurrency,
      this.state.userToTransfer,
      function(err, res) {
        if (err) {
          Bert.alert(err.reason, 'danger');
        } else {
          Bert.alert("Successfully transfered funds to user", 'success');
          this.setState({selectedCurrency: "", userToTransfer: "", amount: 0});
        }
      }.bind(this)
    )
  }

  withdraw() {}

  getEarnings() {
    let platformEarnings = PlatformEarnings.find({}).fetch();

    //Group earnings by currency
    let earnings = groupEarningsByCurrency(platformEarnings);

    //Get extra crypto currency information and filter by search text
    let finalEarnings = [];
    let searchText = this.state.currencySearch;
    earnings.forEach((e) => {
      let cryptoCurrency = CryptoCurrencies.findOne({id: e.currency});
      if (searchText == "" || cryptoCurrency.name.toLowerCase().indexOf(searchText.toLowerCase()) != -1 || cryptoCurrency.code.toLowerCase().indexOf(searchText.toLowerCase()) != -1)
        finalEarnings.push({
          ...e,
          ...cryptoCurrency
        });
      }
    )

    return finalEarnings.map((fE, index) => {
      return (
        <div className="bo-currency-container" key={index} onClick={() => this.setState({selectedCurrency: fE.id})}>
          <img src={fE.imageUrl} alt={fE.name} style={{
              width: 40
            }}/>
          <h4>{fE.name}</h4>
          <div className="bo-currency-values">
            <span>{`Amount: ${fE.amount}`}</span>
            <span>{`Value: ${convertToUserCurrency(fE.currentValue)}${getUserCurrencySymbol()}`}</span>
            <span>{`Total: ${convertToUserCurrency(fE.amount * fE.currentValue)}${getUserCurrencySymbol()}`}</span>
          </div>
        </div>
      );
    });
  }

  render() {
    return (
      <div>
        <Col xs={12} sm={12} md={6} lg={6}>
          <h1>Platform Earnings</h1>
          <Table
            useFixedHeader={true}
            columns={columns}
            data={this.getPlatformEarningsData()}
            rowKey="timestamp"
            scroll={{
              x: false,
              y: 600
            }}/>
        </Col>
        <Col xs={12} sm={12} md={6} lg={3}>
          <h1>Totals</h1>
          <Col sm={6}>
            <h5>Amount</h5>
            <FormGroup className="bo-currency-action-container">
              <Input
                type="text"
                value={this.state.amount}
                placeholder="0"
                onChange={(event) => this.setState({amount: event.target.value})}/>
            </FormGroup>
          </Col>
          <Col sm={6}>
            <h5>Search</h5>
            <FormGroup className="bo-currency-action-container">
              <Input
                type="text"
                value={this.state.currencySearch}
                placeholder="Search Text"
                onChange={(event) => this.setState({currencySearch: event.target.value})}/>
            </FormGroup>
          </Col>
          {
            this.state.selectedCurrency
              ? <div>
                  <h3 style={{
                      textAlign: "center"
                    }}>{this.state.selectedCurrency}</h3>
                  <Col sm={12}>
                    <FormGroup className="bo-currency-action-container">
                      <Input
                        type="text"
                        value={this.state.userToTransfer}
                        placeholder="Username"
                        onChange={(event) => this.setState({userToTransfer: event.target.value})}/>
                      <Button onClick={() => this.transfer()}>Transfer</Button>
                    </FormGroup>
                    <FormGroup className="bo-currency-action-container">
                      <Input
                        type="text"
                        value={this.state.withdrawAddress}
                        placeholder="Address"
                        onChange={(event) => this.setState({withdrawAddress: event.target.value})}/>
                      <Button>Withdraw</Button>
                    </FormGroup>
                  </Col>
                </div>
              : null
          }
          <Col sm={12}>
            {this.getEarnings()}
          </Col>
        </Col>
        <Col xs={12} sm={12} md={6} lg={3}>
          <h1>Stats</h1>
        </Col>
      </div>
    );
  }
}

BackOffice.propTypes = {
  loading: PropTypes.bool.isRequired
};

export default createContainer(() => {
  const subscriptionPlatformEarnings = Meteor.subscribe('platformEarnings');
  const subscriptionCurrencies = Meteor.subscribe('cryptoCurrencies');

  return {
    loading: !subscriptionPlatformEarnings.ready() || !subscriptionCurrencies.ready(),
    user: Meteor.user()
  };
}, BackOffice);

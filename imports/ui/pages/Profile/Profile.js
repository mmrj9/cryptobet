/* eslint-disable no-underscore-dangle */

import React from 'react';
import PropTypes from 'prop-types';
import {ControlLabel} from 'react-bootstrap';
import {Row, Col, FormGroup, Button} from 'reactstrap';
import _ from 'lodash';
import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';
import {Bert} from 'meteor/themeteorchef:bert';
import {createContainer} from 'meteor/react-meteor-data';
import InputHint from '../../components/InputHint/InputHint';
import validate from '../../../modules/validate';

import './Profile.scss';
import CryptoCurrencies from '../../../api/CryptoCurrencies/CryptoCurrencies';

class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.getUserType = this.getUserType.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.renderProfileForm = this.renderProfileForm.bind(this);
    this.renderWalletsForm = this.renderWalletsForm.bind(this);
    this.state = {
      currencies: []
    };
  }

  componentWillMount() {
    let currencies = CryptoCurrencies.find({});
    this.setState({currencies});
  }

  componentDidMount() {

    const component = this;

    validate(component.form, {
      rules: {
        username: {
          required: true
        },
        emailAddress: {
          required: true,
          email: true
        },
        currentPassword: {
          required() {
            // Only required if newPassword field has a value.
            return component.newPassword.value.length > 0;
          }
        },
        newPassword: {
          required() {
            // Only required if currentPassword field has a value.
            return component.currentPassword.value.length > 0;
          }
        }
      },
      messages: {
        username: {
          required: 'What\'s your username?'
        },
        emailAddress: {
          required: 'Need an email address here.',
          email: 'Is this email address correct?'
        },
        currentPassword: {
          required: 'Need your current password if changing.'
        },
        newPassword: {
          required: 'Need your new password if changing.'
        }
      },
      submitHandler() {
        component.handleSubmit();
      }
    });
  }

  getUserType(user) {
    const userToCheck = user;
    delete userToCheck.services.resume;
    const service = Object.keys(userToCheck.services)[0];
    return service === 'password'
      ? 'password'
      : 'oauth';
  }

  handleSubmit() {
    let wallets = this.props.user.profile.wallets || [];
    this.state.currencies.forEach((currency) => {
      let userWallet = _.find(wallets, {id: currency.id});
      if (this[currency.id + "Address"]) {
        let address = this[currency.id + "Address"].value;
        if (!userWallet && address) {
          let wallet = {
            id: currency.id,
            address: address,
            amount: 0
          }
          wallets.push(wallet);
        }
      }
    });
    const profile = {
      emailAddress: this.emailAddress.value,
      profile: {
        username: this.username.value,
        wallets
      }
    };

    Meteor.call('users.editProfile', profile, (error) => {
      if (error) {
        Bert.alert(error.reason, 'danger');
      } else {
        Bert.alert('Profile updated!', 'success');
      }
    });

    if (this.newPassword.value) {
      Accounts.changePassword(this.currentPassword.value, this.newPassword.value, (error) => {
        if (error) {
          Bert.alert(error.reason, 'danger');
        } else {
          this.currentPassword.value = '';
          this.newPassword.value = '';
        }
      });
    }
  }

  renderProfileForm(loading, user) {
    return !loading
      ? (
        <div>
          <Row>
            <Col xs={12}>
              <FormGroup>
                <ControlLabel>Username</ControlLabel>
                <input
                  type="text"
                  name="username"
                  defaultValue={user.profile.username}
                  ref={username => (this.username = username)}
                  className="form-control"/>
              </FormGroup>
            </Col>
          </Row>
          <FormGroup>
            <ControlLabel>Email Address</ControlLabel>
            <input
              type="email"
              name="emailAddress"
              defaultValue={user.emails[0].address}
              ref={emailAddress => (this.emailAddress = emailAddress)}
              className="form-control"/>
          </FormGroup>
          <FormGroup>
            <ControlLabel>Current Password</ControlLabel>
            <input
              type="password"
              name="currentPassword"
              ref={currentPassword => (this.currentPassword = currentPassword)}
              className="form-control"/>
          </FormGroup>
          <FormGroup>
            <ControlLabel>New Password</ControlLabel>
            <input
              type="password"
              name="newPassword"
              ref={newPassword => (this.newPassword = newPassword)}
              className="form-control"/>
            <InputHint>Use at least six characters.</InputHint>
          </FormGroup>
          <Button type="submit" color="success">Save Profile</Button>
        </div>
      )
      : <div/>;
  }

  renderWalletsForm(loading, user) {
    const result = this.state.currencies.map((currency, index) => {
      let userWallet = _.find(user.profile.wallets, {id: currency.id});
      return (
        <div key={index}>
          <Row>
            <Col xs={12}>
              {userWallet
                ? <FormGroup>
                    <ControlLabel>{`${currency.name} Address`}</ControlLabel>
                    <p>{userWallet.address}</p>
                    <p>{`Deposit to: ${currency.depositAddress}`}</p>
                  </FormGroup>
                : <FormGroup>
                  <ControlLabel>{`${currency.name} Address`}</ControlLabel>
                  <input
                    type="text"
                    name={currency.id + "Address"}
                    defaultValue={null}
                    placeholder={`Please insert a valid ${currency.name} address`}
                    ref={address => (this[currency.id + "Address"] = address)}
                    className="form-control"/>
                </FormGroup>
}
            </Col>
          </Row>
        </div>
      )
    });
    return result;
  }

  render() {
    const {loading, user} = this.props;
    return (
      <div className="Profile">
        <Row>
          <Col xs={12} sm={6} md={4}>
            <h4 className="page-header">Edit Profile</h4>
            <form ref={form => (this.form = form)} onSubmit={event => event.preventDefault()}>
              {this.renderProfileForm(loading, user)}
            </form>
          </Col>
          <Col xs={12} sm={6} md={4}>
            <h4 className="page-header">Wallets</h4>
            <form ref={form => (this.walletsForm = form)} onSubmit={event => event.preventDefault()}>
              {this.renderWalletsForm(loading, user)}
            </form>
          </Col>
        </Row>
      </div>
    );
  }
}

Profile.propTypes = {
  loading: PropTypes.bool.isRequired,
  user: PropTypes.object.isRequired
};

export default createContainer(() => {
  const subscription = Meteor.subscribe('users.editProfile');
  const subscriptionCurrencies = Meteor.subscribe('cryptoCurrencies');
  return {
    loading: !subscription.ready() && !subscriptionCurrencies.ready(),
    user: Meteor.user()
  };
}, Profile);

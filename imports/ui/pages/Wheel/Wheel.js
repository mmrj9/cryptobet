import React from 'react';
import PropTypes from 'prop-types';
import {createContainer} from 'meteor/react-meteor-data';
import {
  Row,
  Col,
  Button,
  FormGroup,
  ControlLabel,
  FormControl,
  HelpBlock,
  ButtonToolbar
} from 'react-bootstrap';
import DonutChart from 'react-donut-chart';
import Countdown from '../../components/Countdown/Countdown';
import TimeAgo from 'react-timeago'
import ConfirmLink from 'react-confirm-dialog';
import moment from 'moment';

import WheelSection from '../../components/Containers/WheelSection';
import PreviousTurn from '../../components/Containers/PreviousTurn';

import './Wheel.scss';
import WheelTurns from '../../../api/WheelTurns/WheelTurns';
import CryptoCurrencies from '../../../api/CryptoCurrencies/CryptoCurrencies';

//helpers
import {convertToUserCurrency, getUserCurrencySymbol, convertDateToUserTimezone} from '../../../helpers/helpers';
import {groupBetsByPlayer, getTotalNumberOfPlayers} from '../../../api/WheelTurns/helpers';

class Wheel extends React.Component {
  constructor(props) {
    super(props);
    this.getTimerOptions = this.getTimerOptions.bind(this);
    this.getWheelData = this.getWheelData.bind(this);
    this.selectCurrency = this.selectCurrency.bind(this);
    this.handleBetAmountChange = this.handleBetAmountChange.bind(this);
    this.isValidUser = this.isValidUser.bind(this);
    this.placeBet = this.placeBet.bind(this);

    this.state = {
      wheelData: null,
      selectedCurrency: null,
      selectedCurrencyAvailableAmount: 0,
      betAmount: 0,
      isValidAmount: false,
      betInputFeedback: ""
    }
  }

  componentWillMount() {
    // console.log("componentWillMount");
  }

  componentReceiveProps() {
    // console.log("componentReceiveProps");
  }

  componentDidUpdate() {
    // console.log("componentWillUpdate");
    let currentTurn = this.props.currentTurn;
    if (currentTurn) {
      if (currentTurn.isFinishing) {
        console.log(this.props);
        var audio = new Audio('/sound/notif.mp3');
        audio.play();
      }
    }
  }

  isValidUser() {
    let user = this.props.user;
    //TODO: uncomment return user && user.emails[0].verified;
    return user;
  }

  getTimerOptions() {
    let currentTurn = this.props.currentTurn;
    if (currentTurn) {
      return {
        endDate: moment(currentTurn.endDate).format("MM/DD/YYYY hh:mm:ss A"),
        prefix: 'left!',
        reset: true,
        cb: () => {}
      }
    } else {
      return {};
    }
  }

  getWheelData() {
    let currentTurn = this.props.currentTurn;
    // console.log(groupBetsByPlayer(currentTurn));
    return groupBetsByPlayer(currentTurn);
  }

  getBetHistory() {
    let currentTurn = this.props.currentTurn;
    let result = null;
    if (currentTurn) {
      let betHistory = _.sortBy(currentTurn.bets, function(num) {
        return num;
      });
      betHistory = betHistory.reverse();
      result = betHistory.map((bet, index) => {
        let placedBets = "";
        let totalValue = 0;
        bet.bets.forEach((b) => {
          if (placedBets === "") {
            placedBets = `${b.amount} ${b.currency}`;
          } else {
            placedBets = ` + ${b.amount} ${b.currency}`;
          }
          totalValue = (totalValue + b.value).toFixed(2);
        })
        return (
          <div key={index} className="bet-history-container">
            <span className="bet-history-description">{`@${bet.user} placed a bet of ${placedBets}. Total Value: ${convertToUserCurrency(totalValue)}${getUserCurrencySymbol()}`}</span>
            <TimeAgo date={convertDateToUserTimezone(bet.timestamp)} className="bet-history-timestamp"/>
          </div>
        )
      })
    }
    return result;
  }

  getLastTurns() {
    let lastTurns = this.props.lastTurns;
    result = lastTurns.map((turn, index) => {
      return (<PreviousTurn turn={turn} key={index}/>)
    });
    return result;
  }

  selectCurrency(currency) {
    let userWallets = this.props.user.profile.wallets;
    let wallet = _.findWhere(userWallets, {id: currency.id});
    this.setState({selectedCurrency: currency, selectedCurrencyAvailableAmount: wallet.amount});
  }

  getValidationState() {
    const amount = this.state.betAmount;
    const selectedCurrency = this.state.selectedCurrency;
    let betInputFeedback = "";
    if (isNaN(amount)) {
      betInputFeedback = "Invalid amount";
      if (betInputFeedback != this.state.betInputFeedback)
        this.setState({betInputFeedback, isValidAmount: false})
      return 'error';
    } else if (amount <= 0) {
      betInputFeedback = "The amount must be greater than 0";
      if (betInputFeedback != this.state.betInputFeedback)
        this.setState({betInputFeedback, isValidAmount: false})
      return 'warning';
    } else if (amount > 0) {
      let userWallets = this.props.user.profile.wallets;
      let wallet = _.findWhere(userWallets, {id: selectedCurrency.id});
      if (!wallet || wallet.amount < amount) {
        betInputFeedback = `You don't have enough ${selectedCurrency.name} deposited into your wallet.`;
        if (betInputFeedback != this.state.betInputFeedback)
          this.setState({betInputFeedback, isValidAmount: false})
        return 'error';
      } else {
        let totalValue = 0;
        this.props.currentTurn.bets.forEach((bet) => {
          bet.bets.forEach((b) => {
            totalValue = totalValue + b.value;
          });
        });
        let aproxValue = amount * selectedCurrency.currentValue;
        totalValue = totalValue + aproxValue;
        let aproxProbability = (aproxValue * 100) / totalValue;
        let betInputFeedback = `Aprox: ${convertToUserCurrency(aproxValue)}${getUserCurrencySymbol()} (~${aproxProbability.toFixed(
          2
        )}%)`;
        if (betInputFeedback != this.state.betInputFeedback)
          this.setState({betInputFeedback, isValidAmount: true})
        return 'success';
      }
    }
    // else if (amount <= 0)   return 'error';
  }

  handleBetAmountChange(e) {
    let value = e.target.value;
    value = value.replace(",", ".");
    if (Number(value)) {
      let dotIndex = value.indexOf(".") + 1;
      value = value.substring(0, dotIndex + 8);
    }
    this.setState({betAmount: value});
  }

  getBettingCurrencies() {
    let user = this.props.user;
    let currencies = this.props.currencies;
    let result = null;
    if (user) {
      let wallets = user.profile.wallets || [];

      result = wallets.map((wallet, index) => {
        let currency = _.findWhere(currencies, {id: wallet.id});
        if (currency) {
          let selectedCurrency = this.state.selectedCurrency;
          return (
            <div
              className={(
                selectedCurrency && currency.id == selectedCurrency.id)
                ? "betting-option-container-active"
                : "betting-option-container"}
              key={index}
              onClick={() => this.selectCurrency(currency)}><img src={currency.imageUrl} alt={currency.name}/>
              <h4>{currency.code}</h4>
            </div>
          )
        }
      });
    }
    return result;
  }

  placeBet() {
    const user = this.props.user;
    const currencyId = this.state.selectedCurrency.id;
    const amount = this.state.betAmount;

    Meteor.call("place.bet", user._id, [
      {
        currencyId,
        amount
      }
    ], function(err, res) {
      if (res) {
        Bert.alert("Your bet was placed!", 'success');
        this.setState(
          {selectedCurrency: null, selectedCurrencyAvailableAmount: 0, betAmount: 0, isValidAmount: false, betInputFeedback: ""}
        );
      } else {
        Bert.alert("Error placing bet. Please try again.", 'danger');
      }
    }.bind(this))

  }

  render() {
    const selectedCurrency = this.state.selectedCurrency;
    const currentTurn = this.props.currentTurn;
    return (
      <div>
        <Col xs={12} sm={12} md={4} lg={4}>
          <WheelSection className="how-it-works-wrapper" title="How It Works">
            <ul>
              <li>1</li>
              <li>2</li>
              <li>3</li>
              <li>4</li>
            </ul>
          </WheelSection>
          <WheelSection className="last-turns-wrapper" title="Previous Turns">
            {this.getLastTurns()}
          </WheelSection>
        </Col>
        <Col xs={12} sm={12} md={4} lg={4} className="text-center" id="donut-container">
          {
            currentTurn
              ? <div>{
                    !currentTurn.isFinishing
                      ? <div>
                          <h3>{`${getTotalNumberOfPlayers(currentTurn)}/${WHEEL_MAX_NUMBER_OF_PLAYERS} Players`}</h3><Countdown options={this.getTimerOptions()}/></div>
                      : <h2 className="react-count-down">Ending turn...</h2>
                  }
                  {
                    currentTurn.isFinishing
                      ? <div></div>
                      : <DonutChart
                          legend={false}
                          width={document.getElementById("donut-container")
                            ? document.getElementById("donut-container").offsetWidth
                            : 300}
                          data={this.getWheelData()}
                          onMouseEnter={(item) => {
                            console.log(`mousing over: ${item.label}`);
                            return item;
                          }}
                          onClick={(item, selected) => {
                            if (selected) {
                              console.log(`selecting: ${item.label}`);
                            } else {
                              console.log(`unselecting: ${item.label}`);
                            }
                            return item;
                          }}/>
                  }</div>
              : null
          }
        </Col>
        <Col xs={12} sm={12} md={4} lg={4}>
          <WheelSection className="betting-menu-wrapper" title="Place Bet">
            {
              this.isValidUser()
                ? <div>
                    <div className="betting-options-wrapper">{this.getBettingCurrencies()}</div>
                    <div className="text-center">{
                        selectedCurrency
                          ? <div>
                              <h4>{`You are placing an ${selectedCurrency.name} bet`}</h4>
                              <h5 onClick={() => this.setState({betAmount: this.state.selectedCurrencyAvailableAmount})}>{`Available: ${this.state.selectedCurrencyAvailableAmount} ${selectedCurrency.code}`}</h5>
                              <form>
                                <FormGroup controlId="formBasicText" validationState={this.getValidationState()}>
                                  <div className="text-center bet-input-container">
                                    <FormControl
                                      autoComplete="off"
                                      type="text"
                                      value={this.state.betAmount}
                                      placeholder="0.00"
                                      onChange={this.handleBetAmountChange}/>
                                    <FormControl.Feedback/>
                                  </div>
                                  <HelpBlock>{this.state.betInputFeedback}</HelpBlock>
                                </FormGroup>{
                                  this.state.isValidAmount
                                    ? <ConfirmLink
                                        action={() => this.placeBet()}
                                        confirmMessage={`Are you sure you want to place a ${this.state.betAmount}${selectedCurrency.code} bet?`}
                                        confirmText={"I'm sure"}
                                        cancelText={"Not really"}>
                                        <Button>Place Bet</Button>
                                      </ConfirmLink>
                                    : <div/>
                                }
                              </form>
                            </div>
                          : <h4>Please choose a currency</h4>
                      }</div>
                  </div>
                : <h4>In order to place a bet you must be logged in a verified account.</h4>
            }
          </WheelSection>
          <WheelSection className="bet-history-wrapper" title="Bet History">
            <div data-simplebar="data-simplebar" style={{
                maxHeight: 210
              }}>
              <div className="bet-histories">
                {this.getBetHistory()}
              </div>
            </div>
          </WheelSection>
        </Col>
      </div>
    );
  }
}

Wheel.propTypes = {
  loading: PropTypes.bool.isRequired
};

export default createContainer(() => {
  console.log("here");
  const subscription = Meteor.subscribe('wheelTurns');
  const subscriptionCurrencies = Meteor.subscribe('cryptoCurrencies');
  return {
    loading: !subscription.ready() && !subscriptionCurrencies.ready(),
    user: Meteor.user(),
    currentTurn: WheelTurns.findOne({isDone: false}),
    lastTurns: WheelTurns.find({
      isDone: true
    }, {
      sort: {
        endDate: -1
      },
      limit: 10
    }).fetch(),
    currencies: CryptoCurrencies.find({}).fetch()
  };
}, Wheel);

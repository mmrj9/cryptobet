import React from 'react';
import {Row, Col} from 'reactstrap';
import FontAwesome from 'react-fontawesome';

class WheelSection extends React.Component {
  constructor(props) {
    super(props);
    this.state= { collapsed: false }
  }

  render() {
    return (
        <Col xs={12} sm={12} md={12} lg={12} className={this.state.collapsed ? "wheel-section wheel-section-collapsed" : "wheel-section"}>
          <h3>{this.props.title}</h3>
          <div><FontAwesome name={this.state.collapsed ? 'chevron-down' : 'chevron-up'} className="expand-icon" onClick={() => this.setState({collapsed: !this.state.collapsed})}/></div>
          {this.props.children}
        </Col>
    );
  }
}
export default WheelSection;

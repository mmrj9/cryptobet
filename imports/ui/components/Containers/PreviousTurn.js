import React from 'react';
import {Row, Col} from 'reactstrap';
import FontAwesome from 'react-fontawesome';

//helpers
import {getTotalWheelTurnValue} from '../../../helpers/helpers';

class PreviousTurn extends React.Component {
  constructor(props) {
    super(props);
    this.state= { collapsed: false }
  }

  render() {
    let turn = this.props.turn;
    return (
        <Col xs={12} sm={12} md={12} lg={12} className={this.state.collapsed ? "previous-turn previous-turn-collapsed" : "previous-turn"}>
          <span>Winner: {turn.winner}</span><br/>
          <span>Total: {getTotalWheelTurnValue(turn.bets)}</span><br/>
          <span>Player's Bets: {getTotalWheelTurnValue(turn.bets, turn.winner)}</span><br/>
        </Col>
    );
  }
}
export default PreviousTurn;

import React from 'react';
import PropTypes from 'prop-types';
import {LinkContainer} from 'react-router-bootstrap';
import {Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';
import {Meteor} from 'meteor/meteor';
import {Roles} from 'meteor/alanning:roles';

const AuthenticatedNavigation = ({name}) => (
  <div>
    <Nav pullRight={true}>
      <NavDropdown eventKey={2} title={name} id="user-nav-dropdown">
        {
          Roles.userIsInRole(Meteor.user(), ['admin'])
            ? <LinkContainer to="/bo">
                <NavItem eventKey={2.1} href="/bo">BackOffice</NavItem>
              </LinkContainer>
            : null
        }
        {
          Roles.userIsInRole(Meteor.user(), ['admin'])
            ? <MenuItem divider={true}/>
            : null
        }
        <LinkContainer to="/profile">
          <NavItem eventKey={2.1} href="/profile">Profile</NavItem>
        </LinkContainer>
        <MenuItem divider={true}/>
        <MenuItem eventKey={2.2} onClick={() => Meteor.logout()}>Logout</MenuItem>
      </NavDropdown>
    </Nav>
  </div>
);

AuthenticatedNavigation.propTypes = {
  name: PropTypes.string.isRequired
};

export default AuthenticatedNavigation;

import {Meteor} from 'meteor/meteor';
import Currencies from '../Currencies';


Meteor.publish('currencies', () => {
  return Currencies.find({});
});

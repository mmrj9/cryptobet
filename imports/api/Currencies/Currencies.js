import _ from 'underscore';
import currencyData from './CurrencyData';

const Currencies = new Mongo.Collection('Currencies');

Currencies.allow({
  insert: () => false,
  update: () => false,
  remove: () => false
});

Currencies.deny({
  insert: () => true,
  update: () => true,
  remove: () => true
});

const openexchangeratesAPI = "https://openexchangerates.org/api/";

if (Meteor.isServer) {
  Meteor.startup(function() {
    if (!Currencies.findOne({})) {
      let keys = Object.keys(currencyData);
      keys.forEach((key) => {
        Currencies.insert(currencyData[key]);
      })
    }
    Meteor.setInterval(function() {
      try {
        //Get the latest exchange rates available from the Open Exchange Rates API.
        const result = HTTP.call(
          'GET',
          openexchangeratesAPI + 'latest.json?app_id=' + Meteor.settings.private.OPEN_EXCHANGE_RATES.appId
        );
        if (result.data && result.data.rates) {
          let rates = result.data.rates;
          let keys = Object.keys(rates);
          keys.forEach((key) => {
            let c = Currencies.findOne({code: key});
            if (c) {
              c.rate = rates[key];
              Currencies.update(c._id, c);
            }
          });
          Currencies.remove({rate: null});
        }
      } catch (e) {
        let log = {
          isError: true,
          type: "API",
          description: "Error GET - " + openexchangeratesAPI + 'latest.json?app_id=',
          data: e
        }
        Meteor.call("log", log);
      }
    }, 3600000);
    //1 hour interval
  });
}

export default Currencies;

import PlatformEarnings from '../PlatformEarnings';
import {Meteor} from 'meteor/meteor';
import {Roles} from 'meteor/alanning:roles';

import {groupEarningsByCurrency, hasFunds} from '../helpers';

Meteor.startup(() => {
  Meteor.methods({
    "add.earnings": function(earnings) {
      return PlatformEarnings.insert({
        ...earnings,
        timestamp: new Date()
      });
    },
    /*
    TODO: Needs more verifications. Ex: plaftform has enought funds to transfer, currency exists
     */
    "transfer.to.user": function(amount, currencyId, username) {
      if (Roles.userIsInRole(Meteor.user(), ['admin'])) {
        if (!Number(amount) || Number(amount) < 0 || !hasFunds(amount, currencyId)) {
          throw new Meteor.Error(1002, "Invalid Amount");
          return;
        }
        let user = Meteor.users.findOne({"profile.username": username});
        if (user) {
          //Credit funds to user
          Meteor.call("user.credit.funds", user._id, currencyId, Number(amount));

          //Decrement money from platform earnings
          let newPlatformEarning = {
            "action": "transfer",
            "data": username,
            "earnings": [
              {
                "currency": currencyId,
                "amount": amount * -1
              }
            ],
            "timestamp": new Date()
          }
          PlatformEarnings.insert(newPlatformEarning);
        } else {
          throw new Meteor.Error(1004, "User Not Found");
        }
      } else {
        throw new Meteor.Error(1001, "Access Denied");
      }
    }
  });
});

import {Meteor} from 'meteor/meteor';
import PlatformEarnings from '../PlatformEarnings';

Meteor.publish('platformEarnings', () => {
  return PlatformEarnings.find({}, {
    sort: {
      timestamp: -1
    }
  });
});

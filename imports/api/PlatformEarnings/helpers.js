import _ from 'underscore';

import PlatformEarnings from './PlatformEarnings';

const groupEarningsByCurrency = (platformEarnings) => {
  let earnings = [];
  //Group earnings by currency
  platformEarnings.forEach((pE) => {
    pE.earnings.forEach((e) => {
      let existingEarningCurrency = _.findWhere(earnings, {currency: e.currency});
      if (existingEarningCurrency) {
        existingEarningCurrency.amount = Number((existingEarningCurrency.amount + e.amount).toFixed(maxDecimalPlaces));
        _.extend(_.findWhere(earnings, {currency: e.currency}), existingEarningCurrency);
      } else {
        earnings.push(e);
      }
    });
  });
  return earnings;
}

const hasFunds = (amount, currencyId) => {
  let earnings = groupEarningsByCurrency(PlatformEarnings.find({"earnings.currency": currencyId}).fetch());
  let earningsFromCurrency = _.findWhere(earnings, {currency: currencyId});
  return earningsFromCurrency.amount >= amount;
}

export {
  groupEarningsByCurrency,
  hasFunds
};

const CryptoCurrencies = new Mongo.Collection('CryptoCurrencies');

CryptoCurrencies.allow({
  insert: () => false,
  update: () => false,
  remove: () => false
});

CryptoCurrencies.deny({
  insert: () => true,
  update: () => true,
  remove: () => true
});

export default CryptoCurrencies;

import {Meteor} from 'meteor/meteor';
import CryptoCurrencies from '../CryptoCurrencies';


Meteor.publish('cryptoCurrencies', () => {
  return CryptoCurrencies.find({}, {
    fields: {
      id: 1,
      name: 1,
      code: 1,
      depositAddress: 1,
      currentValue: 1,
      imageUrl: 1
    }
  });
});

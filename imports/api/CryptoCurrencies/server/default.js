import _ from 'underscore';

import CryptoCurrencies from '../CryptoCurrencies';

const data = [
  {
    id: "ethereum",
    name: "Ethereum",
    code: "ETH",
    depositAddress: "0xbbcfc1b74617b14e0158de7f9a6c08114e20788b",
    currentValue: "",
    imageUrl: "http://files.coinmarketcap.com.s3-website-us-east-1.amazonaws.com/static/img/coins/200x200/ethereum.png",
    apiTx: "https://etherchain.org/api/tx/",
    apiTxDataPath: "jsonContent.data[0].sender"
  }, {
    id: "ethereum-classic",
    name: "Ethereum Classic",
    code: "ETC",
    depositAddress: "0x7907db3e594f570441c2e4c6d847e6c26c7c5bff",
    currentValue: "",
    imageUrl: "http://files.coinmarketcap.com.s3-website-us-east-1.amazonaws.com/static/img/coins/200x200/ethereum-classic.png",
    apiTx: "https://etcchain.com/gethProxy/eth_getTransactionByHash?txHash=",
    apiTxDataPath: "jsonContent.from"
  }, {
    "_id": "hWgPShR8dy3bae9Wa",
    "id": "bitcoin",
    "name": "Bitcoin",
    "code": "BTC",
    "depositAddress": "1NeU1GAmFwbZawrmrx4zxhHDqLjpHXn8j7",
    "currentValue": "",
    "imageUrl": "https://bitcoin.org/img/icons/opengraph.png",
    "apiTx": "https://chain.so/api/v2/get_tx/BTC/",
    "apiTxDataPath": "jsonContent.data.inputs@address"
  }
];

const coinmarketcapAPI = "https://api.coinmarketcap.com/v1/ticker/xxx";

if (Meteor.isServer) {
  Meteor.startup(function() {
    data.forEach((d) => {
      let existingCrypto = CryptoCurrencies.findOne({id: d.id});
      if (!existingCrypto) {
        try {
          const request = coinmarketcapAPI.replace("xxx", d.id);
          HTTP.call('GET', request, {}, function(error, coinRes) {
            if (error) {
              let log = {
                isError: true,
                type: "API",
                description: "Error CoinMarketCap GET - request",
                data: error
              }
              Meteor.call("log", log);
            } else {
              let jsonContent = JSON.parse(coinRes.content);
              d.currentValue = Number(jsonContent[0].price_usd);
              CryptoCurrencies.insert(d);
            }
          });
        } catch (e) {
          let log = {
            isError: true,
            type: "API",
            description: "Error CoinMarketCap GET - request",
            data: e
          }
          Meteor.call("log", log);
        }
      }
    });

    //Process - Update Crypto Current Value. 10 minutes - 600000 ms
    Meteor.setInterval(function() {
      let cryptos = CryptoCurrencies.find({}).fetch();
      cryptos.forEach((crypto) => {
        try {
          const request = coinmarketcapAPI.replace("xxx", crypto.id);
          HTTP.call('GET', request, {}, function(error, coinRes) {
            if (error) {
              let log = {
                isError: true,
                type: "API",
                description: "Error CoinMarketCap GET - request",
                data: error
              }
              Meteor.call("log", log);
            } else {
              let jsonContent = JSON.parse(coinRes.content);
              let currentValue = Number(jsonContent[0].price_usd);
              if (currentValue) {
                CryptoCurrencies.update(crypto._id, {
                  $set: {
                    currentValue: currentValue
                  }
                });
              }
            }
          });
        } catch (e) {
          let log = {
            isError: true,
            type: "API",
            description: "Error CoinMarketCap GET - request",
            data: e
          }
          Meteor.call("log", log);
        }
      });
    }, 600000);

  });
}

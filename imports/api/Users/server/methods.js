import {Meteor} from 'meteor/meteor';
import {Accounts} from 'meteor/accounts-base';
import editProfile from './edit-profile';
import rateLimit from '../../../modules/rate-limit';

Meteor.methods({
  'users.sendVerificationEmail': function usersResendVerification(username) {
    let userId = Meteor.users.findOne({"profile.username": username})._id;
    return Accounts.sendVerificationEmail(userId);
  },
  'users.editProfile': function usersEditProfile(profile) {
    return editProfile({userId: this.userId, profile}).then(response => response).catch((exception) => {
      throw new Meteor.Error('500', exception);
    });
  },
  'user.credit.funds': (userId, currencyId, amount) => {
    let user = Meteor.users.findOne(userId);
    let userWallets = user.profile.wallets
      ? user.profile.wallets
      : [];
    //search for user wallet related to the bet currency
    let wallet = _.findWhere(userWallets, {id: currencyId});
    //if wallet already exists
    if (wallet) {
      //credit bet amount - fee
      wallet.amount = Number((Number(wallet.amount) + amount).toFixed(maxDecimalPlaces));
      //repace with new value
      _.extend(_.findWhere(userWallets, {id: currencyId}), wallet);
    } else {
      let newWallet = {
        id: currencyId,
        address: null,
        amount: Number(amount.toFixed(maxDecimalPlaces))
      }
      //insert new wallet without address
      userWallets.push(newWallet);
    }

    //Update winner user wallets with turn gains
    return Meteor.users.update({
      _id: user._id
    }, {
      $set: {
        "profile.wallets": userWallets
      }
    });
  }
});

rateLimit({
  methods: [
    'users.sendVerificationEmail', 'users.editProfile'
  ],
  limit: 5,
  timeRange: 1000
});

import { Meteor } from 'meteor/meteor';
import getPrivateFile from '../../../modules/server/get-private-file';
import parseMarkdown from '../../../modules/parse-markdown';

Meteor.methods({
  'utility.getPage': function utilityGetPage(fileName) {
    return parseMarkdown(getPrivateFile(`pages/${fileName}.md`));
  },
});

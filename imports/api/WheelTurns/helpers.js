import _ from 'underscore';

import WheelTurns from './WheelTurns';

const groupBetsByPlayer = (turn) => {
  let data = [];
  if (turn) {
    turn.bets.forEach((bet) => {
      let d = _.findWhere(data, {label: bet.user});
      let alreadyExists = false;
      let value = 0;
      if (!d) {
        d = {
          label: bet.user,
          value: value
        }
      } else {
        alreadyExists = true;
        value = d.value;
      }

      bet.bets.forEach((bet) => {
        value = value + bet.value;
      })
      d.value = value;

      if (alreadyExists) {
        _.extend(_.findWhere(data, {label: bet.user}), d);
      } else {
        data.push(d);
      }

    });
  }
  return data;
}

const getTurnEarnings = (turn) => {
  let earnings = [];

  turn.bets.forEach((bet) => {
    bet.bets.forEach((b) => {
      let existingEarning = _.findWhere(earnings, {currency: b.currency});
      if (existingEarning) {
        existingEarning.amount = Number((existingEarning.amount + (b.amount * (1 - WHEEL_FEE))).toFixed(maxDecimalPlaces));
        existingEarning.value = Number((existingEarning.value + (b.value * (1 - WHEEL_FEE))).toFixed(2));
        _.extend(_.findWhere(earnings, {currency: b.currency}), existingEarning);
      } else {
        earnings.push(b);
      }
    });
  });
  return earnings;
}

const getTotalNumberOfPlayers = (turn) => {
  if (!turn)
    return 0;

  let players = [];
  turn.bets.forEach((bet) => {
    //if player not in the list
    if (players.indexOf(bet.user) === -1) {
      players.push(bet.user);
    }
  });

  return players.length;
}

export {
  getTurnEarnings,
  groupBetsByPlayer,
  getTotalNumberOfPlayers
};

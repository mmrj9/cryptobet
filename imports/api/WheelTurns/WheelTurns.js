import _ from 'underscore';
import moment from 'moment';
import {Email} from 'meteor/email'

import CryptoCurrencies from '../CryptoCurrencies/CryptoCurrencies';

//helpers
import {getTurnEarnings, groupBetsByPlayer, getTotalNumberOfPlayers} from './helpers';

const WheelTurns = new Mongo.Collection('WheelTurns');

WheelTurns.allow({
  insert: () => false,
  update: () => false,
  remove: () => false
});

WheelTurns.deny({
  insert: () => true,
  update: () => true,
  remove: () => true
});

/*
Generate player list to feed the Weighted List algorithm
{
player
totalBetsValue
}
 */
const getPlayersListWeight = (currentTurn) => {
  let data = groupBetsByPlayer(currentTurn);

  let list = data.map(function(a) {
    return a.label;
  });
  let weights = data.map(function(a) {
    return a.value;
  });

  return {list, weights};
}

const rand = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

/*
Generate Weighted
[A,A,A,A,B,B,C,D]
 */
const generateWeightedList = (list, weight) => {
  var weighted_list = [];

  // Loop over weights
  for (var i = 0; i < weight.length; i++) {
    var multiples = weight[i] * 100;

    // Loop over the list of items
    for (var j = 0; j < multiples; j++) {
      weighted_list.push(list[i]);
    }
  }

  return weighted_list;
};

if (Meteor.isServer) {
  import Future from 'fibers/future';

  Meteor.startup(function() {
    //If there aren't any created turns, insert one
    if (WheelTurns.find({}).count() === 0) {
      let newTurn = {
        isDone: false,
        isFinishing: false,
        startDate: moment().toDate(),
        endDate: moment().add(WHEEL_DEFAULT_TURN_DURATION, 'm').toDate(),
        bets: [],
        winner: null
      }
      WheelTurns.insert(newTurn);
    }

    Meteor.setInterval(function() {
      //get current turn
      let currentWheelTurn = WheelTurns.findOne({isDone: false});
      let currentDate = new Date();
      //check if turn should Finish
      if (currentWheelTurn && (currentDate >= currentWheelTurn.endDate || (getTotalNumberOfPlayers(currentWheelTurn) >= WHEEL_MAX_NUMBER_OF_PLAYERS)) && !currentWheelTurn.isFinishing) {
        //Get player list
        let data = getPlayersListWeight(currentWheelTurn);
        //Check if only one or less players participated
        if (data.list.length <= 1) {
          //Postpone turn end date
          let newEndDate = moment(currentWheelTurn.endDate).add(WHEEL_DEFAULT_POSTPONE_DURATION, 'm').toDate();
          WheelTurns.update(currentWheelTurn._id, {
            $set: {
              endDate: newEndDate
            }
          });
        } else {
          //set isFinishing to true
          WheelTurns.update(currentWheelTurn._id, {
            $set: {
              isFinishing: true
            }
          });
          //Generate Weighted List
          var weighed_list = generateWeightedList(data.list, data.weights);
          //Generated random index
          var random_num = rand(0, weighed_list.length - 1);
          //Select winner using random index in the weighted list
          let winner = weighed_list[random_num];

          //Finish turn and credit gains into the winner's wallet
          currentWheelTurn.winner = winner;
          currentWheelTurn.isFinishing = false;
          currentWheelTurn.isDone = true;
          let winnerUser = Meteor.users.findOne({"profile.username": winner});

          setTimeout(Meteor.bindEnvironment(() => {
            WheelTurns.update(currentWheelTurn._id, currentWheelTurn);
            //Create new turn
            let newTurn = {
              isDone: false,
              isFinishing: false,
              startDate: moment().toDate(),
              endDate: moment().add(WHEEL_DEFAULT_TURN_DURATION, 'm').toDate(),
              bets: [],
              winner: null
            }
            WheelTurns.insert(newTurn);

            //platform earnings
            let platformEarnings = {
              action: WHEEL_ID,
              data: currentWheelTurn._id,
              earnings: []
            }

            if (!winnerUser) {
              let log = {
                isError: false,
                type: "NOT-FOUND",
                description: `Couldn't find a user with the username ${winner}`,
                data: currentWheelTurn
              }
              Meteor.call("log", log);
            } else {
              currentWheelTurn.bets.forEach((bet) => {
                bet.bets.forEach((b) => {
                  let cryptoCurrency = CryptoCurrencies.findOne({code: b.currency});
                  if (!cryptoCurrency) {
                    let log = {
                      isError: false,
                      type: "NOT-FOUND",
                      description: `Couldn't find a crypto currency with the id ${b.currency}`,
                      data: currentWheelTurn
                    }
                    Meteor.call("log", log);
                  } else {
                    //Credit earnings to winner
                    let future = new Future();
                    Meteor.call(
                      "user.credit.funds",
                      winnerUser._id,
                      cryptoCurrency.id,
                      (Number(b.amount) * (1 - WHEEL_FEE)),
                      (err, res) => {
                        future.return (res);
                      }
                    );
                    future.wait();

                    //Platform Earnings
                    let earningByCurrency = _.findWhere(platformEarnings.earnings, {currency: cryptoCurrency.id});
                    //Group earnings by currency
                    if (earningByCurrency) {
                      earningByCurrency.amount = Number(
                        (Number(earningByCurrency.amount) + (Number(b.amount) * WHEEL_FEE)).toFixed(maxDecimalPlaces)
                      );
                      _.extend(_.findWhere(platformEarnings.earnings, {id: cryptoCurrency.id}), earningByCurrency);
                    } else {
                      let newEarningByCurrency = {
                        currency: cryptoCurrency.id,
                        amount: Number(((Number(b.amount) * WHEEL_FEE)).toFixed(maxDecimalPlaces))
                      }
                      platformEarnings.earnings.push(newEarningByCurrency);
                    }
                  }
                });
              });

              //Generate and Send email to winner
              let turnEarnings = getTurnEarnings(currentWheelTurn);
              let to = winnerUser.emails[0].address;
              let from = Meteor.settings.public.MAIL.support;
              let subject = EMAIL_SUBJECT_WIN;
              let html = "<h1>Contratulations!</h1><h4>Your earnings:</h4>";
              let earningsList = "<ul>";
              turnEarnings.forEach((e) => {
                earningsList = earningsList + `<li>${e.amount} ${e.currency} (${e.value}${defaultCurrencySymbol})</li>`;
              });
              html = html + earningsList + "</ul>";
              Email.send({to, from, subject, html});
            }

            //Add to Platform earnings
            Meteor.call("add.earnings", platformEarnings);
          }), WHEEL_TURN_NEW_TURN_DELAY);
        }
      }
    }, 1000);
  });
}

export default WheelTurns;

import {Meteor} from 'meteor/meteor';
import WheelTurns from '../WheelTurns';

Meteor.publish('wheelTurns', () => {
  return WheelTurns.find({});
});

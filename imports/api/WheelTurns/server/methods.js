import {Meteor} from 'meteor/meteor';
import WheelTurns from '../WheelTurns';
import CryptoCurrencies from '../../CryptoCurrencies/CryptoCurrencies';

Meteor.methods({
  "place.bet": (userId, betOffers) => {
    const user = Meteor.users.findOne(userId);

    if (user) {
      let bet = {
        user: user.profile.username,
        bets: [],
        timestamp : new Date()
      };
      betOffers.forEach((offer) => {
        const wallet = _.findWhere(user.profile.wallets, {id: offer.currencyId});
        if (wallet && wallet.amount >= offer.amount) {
          const currency = CryptoCurrencies.findOne({id: offer.currencyId});
          if (currency) {
            bet.bets.push({
              currency: currency.code,
              amount: Number(offer.amount),
              value: offer.amount * currency.currentValue,
            });
            wallet.amount = wallet.amount - offer.amount;
            _.extend(_.findWhere(user.profile.wallets,  {id: offer.currencyId}), wallet);
            Meteor.users.update(user._id, {
              $set: {
                "profile.wallets": user.profile.wallets
              }
            });
          }
        }
      });
      let currentTurn = WheelTurns.findOne({isDone: false});
      let currentTurnBets = currentTurn.bets;
      console.log(bet);
      currentTurnBets.push(bet);

      return WheelTurns.update(currentTurn._id, {
        $set: {
          bets: currentTurnBets
        }
      });
    }
  }
});

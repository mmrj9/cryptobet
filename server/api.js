import Currencies from '../imports/api/Currencies/Currencies';

Meteor.startup(function() {
  // Global API configuration
  let Api = new Restivus({useDefaultAuth: true, prettyJson: true});

  Api.addCollection(Currencies);

  // Maps to: /api/users/:id
  Api.addRoute('users/:id', {
    authRequired: true
  }, {
    get: function() {
      let user = Meteor.users.findOne(this.urlParams.id);
      if (user)
        return user;
      else
        return {};
    }
  });
});

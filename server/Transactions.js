import _ from 'underscore';

import CryptoCurrencies from '../imports/api/CryptoCurrencies/CryptoCurrencies';
import Currencies from '../imports/api/Currencies/Currencies';
import apiQuery from './cryptopia';

const etherchainAPI = 'https://etherchain.org/api/';

Transactions = new Mongo.Collection('Transactions');

Meteor.startup(() => {
  //RESET Transactions.remove({}); let users = Meteor.users.find({}).fetch();
  //
  // users.forEach((user) => {   console.log("reset");   let wallets = user.profile.wallets;   if (wallets) {     let
  // tempWallets = [];     wallets.forEach((wallet) => {       wallet.amount = 0;       tempWallets.push(wallet);     });
  // user.profile.wallets = tempWallets;     Meteor.users.update(user._id, user);   } });
  //
  //
  Meteor.setInterval(function() {
    apiQuery(Meteor.bindEnvironment(function(err, res) {
      let transactionsResponse = null;
      try {
        if (!err) {
          transactionsResponse = JSON.parse(res);
        }
      } catch (e) {
        console.log(e);
      }
      if (err || !transactionsResponse || transactionsResponse.Success != true) {
        let log = {
          isError: true,
          type: "API",
          description: "Error calling cryptopia api GetTransactions method",
          data: err
            ? err
            : transactionsResponse
        }
        Meteor.call("log", log);
      } else {
        let data = transactionsResponse.Data || [];
        const cryptos = CryptoCurrencies.find({}).fetch();
        cryptos.forEach((crypto) => {
          filteredData = _.where(data, {Currency: crypto.code});
          filteredData.forEach((d) => {
            const existingTransaction = Transactions.findOne({TxId: d.TxId});
            if (!existingTransaction) {
              if (d.Type == "Deposit") {
                try {
                  HTTP.call('GET', crypto.apiTx + d.TxId, {}, function(error, txRes) {
                    if (error || txRes.statusCode != 200) {
                      // console.log(crypto.apiTx + d.TxId); console.log(error);
                      let log = {
                        isError: true,
                        type: "API",
                        description: "Error GET - " + crypto.apiTx + d.TxId,
                        data: error
                          ? error
                          : txRes
                      }
                      Meteor.call("log", log);
                    } else {
                      let jsonContent = JSON.parse(txRes.content);
                      if (crypto.apiTxDataPath.indexOf('@') === -1) {
                        d.Address = eval(crypto.apiTxDataPath);
                      } else {
                        let arrayField = crypto.apiTxDataPath.split("@")[0];
                        let addressField = crypto.apiTxDataPath.split("@")[1];
                        console.log(jsonContent);
                        console.log(arrayField);
                        console.log(eval(arrayField));
                        let inputArray = eval(arrayField);
                        inputArray.forEach((input) => {
                          let address = eval(`input.${addressField}`);
                          console.log(address);
                          let user = Meteor.users.findOne({"profile.wallets.address": address});
                          if (user) {
                            d.Address = address;
                          }
                        });
                      }
                      d.completed = false;
                      let newTransactionId = Transactions.insert(d);
                      if (!d.address) {
                        let log = {
                          isError: false,
                          type: "NOT-FOUND",
                          description: `Couldn't find a user with a wallet address ${d.Address} to credit transaction ${d.TxId}`,
                          data: d
                        }
                        Meteor.call("log", log);
                      }
                    }
                  });
                } catch (e) {
                  let log = {
                    isError: true,
                    type: "API",
                    description: "Error GET - " + crypto.apiTx + d.TxId,
                    data: e
                  }
                  Meteor.call("log", log);
                }
              } else if (d.Type == "Withdraw") {
                // console.log("Withdraw");
                d.completed = false;
                let newTransactionId = Transactions.insert(d);
                //temp (for tests)
                d.Address = "0x52bc44d5378309ee2abf1539bf71de1b7d7be3b5";
                let user = Meteor.users.findOne({"profile.wallets.address": d.Address});

                if (user) {
                  let wallets = user.profile.wallets;
                  let walletIndex = _.findIndex(wallets, (wallet) => {
                    return wallet.address == d.Address
                  });
                  let tempWallet = wallets[walletIndex];
                  // console.log(Number(tempWallet.amount).toFixed(maxDecimalPlaces) + " - " + Number(d.Amount).toFixed(maxDecimalPlaces)
                  // + " = " + (Number(tempWallet.amount.toFixed(maxDecimalPlaces) -
                  // d.Amount.toFixed(maxDecimalPlaces))).toFixed(maxDecimalPlaces));
                  tempWallet.amount = Number(
                    (tempWallet.amount.toFixed(maxDecimalPlaces) - d.Amount.toFixed(maxDecimalPlaces)).toFixed(maxDecimalPlaces)
                  );
                  user.profile.wallets[walletIndex] = tempWallet;
                  Meteor.users.update(user._id, user);

                  d.completed = true;
                  Transactions.update(newTransactionId, d);
                }
              } else {
                console.log("Invalid transaction type for tx: " + d.TxId);
              }
            }
          });
        })
      }
    }), 'GetTransactions', {Count: 1000});
    return;
  }, 30000);

  //Handle Deposits
  Meteor.setInterval(function() {
    let uncompletedTransactions = Transactions.find({Type: "Deposit", completed: false}).fetch();

    uncompletedTransactions.forEach((d) => {
      let user = Meteor.users.findOne({"profile.wallets.address": d.Address});
      if (user) {
        // console.log("Deposit");
        let wallets = user.profile.wallets;
        let walletIndex = _.findIndex(wallets, (wallet) => {
          return wallet.address == d.Address
        });
        let tempWallet = wallets[walletIndex];
        // console.log(Number(tempWallet.amount).toFixed(maxDecimalPlaces) + " + " + Number(d.Amount).toFixed(maxDecimalPlaces)
        // + " = " + Number((Number(tempWallet.amount.toFixed(maxDecimalPlaces)) +
        // Number(d.Amount.toFixed(maxDecimalPlaces))).toFixed(maxDecimalPlaces)));
        tempWallet.amount = Number(
          (Number(tempWallet.amount.toFixed(maxDecimalPlaces)) + Number(d.Amount.toFixed(maxDecimalPlaces))).toFixed(maxDecimalPlaces)
        );
        user.profile.wallets[walletIndex] = tempWallet;
        Meteor.users.update(user._id, user);

        d.completed = true;
        Transactions.update(d._id, d);
      } else {
        let log = {
          isError: false,
          type: "NOT-FOUND",
          description: `Couldn't find a user with a wallet address ${d.Address} to credit transaction ${d.TxId}`,
          data: d
        }
        Meteor.call("log", log);
      }
    });
  }, 40000);
});

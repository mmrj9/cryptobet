import {Meteor} from 'meteor/meteor';
import slack from 'slack';

const apiToken = Meteor.settings.private.SLACK.legacyToken;
const slackChannel = Meteor.settings.private.SLACK.channel;

Logs = new Mongo.Collection('Logs');

Meteor.methods({
  /**
   * Input
   * {
   * isError: true/false,
   * type: string,
   * description: string,
   * data: ?????
   * }
  */
  log: function(log) {
    let currDate = new Date();
    //Check if the same log was already logged in the past hour
    let existingLog = Logs.findOne({description: log.description, data: log.data});
    if (existingLog) {
      //if it already exists, update updateAt field and increment the counter
      existingLog.updatedAt = currDate;
      existingLog.count = existingLog.count + 1;
      return Logs.update(existingLog._id, existingLog);
    } else {
      //Post message to slack
      slack.chat.postMessage({
        token: apiToken,
        channel: slackChannel,
        text: log,
        as_user: true
      }, Meteor.bindEnvironment((err, data) => {
        if (err) {
          //Log slack api error
          Logs.insert({
            isError: true,
            type: "API",
            description: "Couldn't send log to slack",
            data: {
              err,
              log
            },
            createdAt: currDate,
            updatedAt: currDate,
            count: 1
          });
        }
      }));
      return Logs.insert({
        ...log,
        createdAt: currDate,
        updatedAt: currDate,
        count: 1
      });
    }
  }
});
